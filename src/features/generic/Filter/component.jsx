import React from 'react';
import PropTypes from 'prop-types';

const Filter = ({ options, onChange, selected }) => (
  <div>
    <ul>
      {options.map(({ label, value }) => (
        <li key={value}>
          <input
            type="checkbox"
            id={value}
            checked={selected.includes(value)}
            onChange={e => {
              // Add or remove value from array depending on checked value
              const nextSelected = e.target.checked
                ? [...selected, value]
                : [...selected].filter(x => x !== value);

              onChange(nextSelected);
            }}
          />
          <label htmlFor={value}>{label || value}</label>
        </li>
      ))}
    </ul>
    <button
      onClick={() => {
        // Passing an empty array will clear all filters
        onChange([]);
      }}
    >
      Clear Filter
    </button>
  </div>
);

Filter.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      value: PropTypes.string.isRequired,
    }),
  ),
  onChange: PropTypes.func,
  selected: PropTypes.arrayOf(PropTypes.string),
};

Filter.defaultProps = {
  options: [],
  onChange: () => {},
  selected: [],
};

export default Filter;
