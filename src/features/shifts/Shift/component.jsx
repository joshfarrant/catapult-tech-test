import React from 'react';
import PropTypes from 'prop-types';

import InvitedContractsContainer from '../../invitedContracts/InvitedContractsContainer';

const Shift = ({ roleId, jobType, shiftDate, startTime, endTime }) => (
  <li>
    Job type: {jobType} <br />
    Start date: {shiftDate} <br />
    Start time: {startTime} <br />
    End time: {endTime} <br />
    <InvitedContractsContainer roleId={roleId} />
    <hr />
  </li>
);

Shift.propTypes = {
  roleId: PropTypes.number.isRequired,
  shiftDate: PropTypes.string.isRequired,
  startTime: PropTypes.string.isRequired,
  endTime: PropTypes.string.isRequired,
  jobType: PropTypes.string.isRequired,
};

export default Shift;
