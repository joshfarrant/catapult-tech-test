import { createSlice } from '@reduxjs/toolkit';

import { fetchShiftsList } from '../../lib/requests/webapi';

const shiftsSlice = createSlice({
  name: 'shifts',
  initialState: {
    shiftsList: {},
    fetchingShifts: false,
  },
  /**
   * We're intentionally mutating the state here
   * https://redux-toolkit.js.org/tutorials/intermediate-tutorial#mutable-update-logic
   */
  /* eslint-disable no-param-reassign */
  reducers: {
    fetchShiftsFulfilled: (state, action) => {
      const {
        payload: { shifts },
      } = action;

      shifts.forEach(shift => {
        state.shiftsList[shift.roleId] = shift;
      });

      state.fetchingShifts = false;
    },
    fetchingShifts: state => {
      state.fetchingShifts = true;
    },
  },
  /* eslint-enable no-param-reassign */
});

export const { fetchShiftsFulfilled, fetchingShifts } = shiftsSlice.actions;

export default shiftsSlice.reducer;

export const fetchShifts = () => async dispatch => {
  dispatch(fetchingShifts());

  try {
    const shifts = await fetchShiftsList();

    const data = {
      shifts: shifts.data,
    };

    dispatch(fetchShiftsFulfilled(data));
  } catch (err) {
    // TODO Add proper error handling
    // eslint-disable-next-line no-console
    console.error('Error fetching shifts: ', err);
  }
};
