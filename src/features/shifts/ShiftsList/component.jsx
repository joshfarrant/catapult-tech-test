import React from 'react';
import PropTypes from 'prop-types';

import Shift from '../Shift';

const ShiftsList = ({ shifts }) => {
  if (shifts.length === 0) {
    return <p>No shifts match selected filters</p>;
  }

  return (
    <ul>
      {shifts.map(shift => (
        <Shift
          key={shift.roleId}
          roleId={shift.roleId}
          jobType={shift.jobType}
          shiftDate={shift.shiftDate}
          startTime={shift.startTime}
          endTime={shift.endTime}
        />
      ))}
    </ul>
  );
};

ShiftsList.propTypes = {
  shifts: PropTypes.arrayOf(
    PropTypes.shape({
      roleId: PropTypes.number.isRequired,
      shiftDate: PropTypes.string.isRequired,
      startTime: PropTypes.string.isRequired,
      endTime: PropTypes.string.isRequired,
      staff_required: PropTypes.number.isRequired,
      number_of_invited_staff: PropTypes.number.isRequired,
      jobType: PropTypes.string.isRequired,
    }),
  ).isRequired,
};

export default ShiftsList;
