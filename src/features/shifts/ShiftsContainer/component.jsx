import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import dedupe from '../../../lib/dedupe';

import ShiftsList from '../ShiftsList';
import Filter from '../../generic/Filter';

const isShiftAm = shift => {
  const startHour = parseInt(shift.startTime.split(':')[0], 10);

  return startHour < 12;
};

const ShiftsContainer = ({ fetchShifts, fetchingShifts, shiftsList }) => {
  useEffect(() => {
    fetchShifts();
  }, []);

  const [visibleJobTypes, setVisibleJobTypes] = useState([]);
  const [visibleStartTimes, setVisibleStartTimes] = useState([]);

  // Create an array of all job types
  const availableJobTypes = dedupe(shiftsList.map(shift => shift.jobType));

  // Get an array of visible shifts based on selected filters
  const visibleShifts = [...shiftsList].filter(shift => {
    const startTime = isShiftAm(shift) ? 'AM' : 'PM';

    const isVislbleType =
      // If no job types are selected, don't filter
      visibleJobTypes.length === 0 || visibleJobTypes.includes(shift.jobType);
    const isVisibleStartTime =
      // If no start times are selected, don't filter
      visibleStartTimes.length === 0 || visibleStartTimes.includes(startTime);

    return isVislbleType && isVisibleStartTime;
  });

  // Could use Suspense here, but it's not production-ready yet
  if (fetchingShifts) {
    return <h2>Loading</h2>;
  }

  return (
    <div>
      <h1>Shifts List</h1>
      <hr />
      <h2>Job Type</h2>
      <Filter
        options={availableJobTypes.map(x => ({ value: x }))}
        selected={visibleJobTypes}
        onChange={setVisibleJobTypes}
      />
      <hr />
      <h2>Start Time</h2>
      <Filter
        options={[{ value: 'AM' }, { value: 'PM' }]}
        selected={visibleStartTimes}
        onChange={setVisibleStartTimes}
      />
      <hr />
      <ShiftsList shifts={visibleShifts} />
    </div>
  );
};

ShiftsContainer.propTypes = {
  shiftsList: PropTypes.arrayOf(
    PropTypes.shape({
      roleId: PropTypes.number.isRequired,
      shiftDate: PropTypes.string.isRequired,
      startTime: PropTypes.string.isRequired,
      endTime: PropTypes.string.isRequired,
      staff_required: PropTypes.number.isRequired,
      number_of_invited_staff: PropTypes.number.isRequired,
      jobType: PropTypes.string.isRequired,
    }),
  ).isRequired,
  fetchingShifts: PropTypes.bool.isRequired,
  fetchShifts: PropTypes.func.isRequired,
};

export default ShiftsContainer;
