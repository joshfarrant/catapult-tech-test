import { connect } from 'react-redux';

import { fetchShifts } from '../shiftsSlice';
import ShiftsContainer from './component';

const mapStateToProps = state => ({
  shiftsList: Object.values(state.shifts.shiftsList),
  fetchingShifts: state.shifts.fetchingShifts,
});

const mapDispatchToProps = dispatch => ({
  fetchShifts: () => dispatch(fetchShifts()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ShiftsContainer);
