import React, { useState } from 'react';
import PropTypes from 'prop-types';

const InvitedContractsContainer = ({
  roleId,
  invitedContractsList,
  fetchInvitedContracts,
}) => {
  const [isLoading, setIsLoading] = useState(false);
  const [hasFetchedContracts, setHasFetchedContracts] = useState(false);

  if (isLoading) {
    return <h2>Loading</h2>;
  }

  if (!hasFetchedContracts) {
    return (
      <button
        onClick={async () => {
          setIsLoading(true);
          try {
            await fetchInvitedContracts(roleId);
          } finally {
            setIsLoading(false);
            setHasFetchedContracts(true);
          }
        }}
      >
        Invited Candidates
      </button>
    );
  }

  const invitedContractsForRole = invitedContractsList.filter(
    invitedContract => invitedContract.roleId === roleId,
  );

  if (invitedContractsForRole.length === 0) {
    return <p>No candidates invited</p>;
  }

  return (
    <ul>
      {invitedContractsForRole.map(invitedContract => (
        <li key={invitedContract.id}>{invitedContract.candidateName}</li>
      ))}
    </ul>
  );
};

InvitedContractsContainer.propTypes = {
  roleId: PropTypes.number.isRequired,
  invitedContractsList: PropTypes.arrayOf(
    PropTypes.shape({
      candidateName: PropTypes.string.isRequired,
    }),
  ),
  fetchInvitedContracts: PropTypes.func.isRequired,
};

InvitedContractsContainer.defaultProps = {
  invitedContractsList: [],
};

export default InvitedContractsContainer;
