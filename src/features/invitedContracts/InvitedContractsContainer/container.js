import { connect } from 'react-redux';

import { fetchInvitedContracts } from '../invitedContractsSlice';
import InvitedContractsContainer from './component';

const mapStateToProps = state => ({
  invitedContractsList: Object.values(
    state.invitedContracts.invitedContractsList,
  ),
});

const mapDispatchToProps = dispatch => ({
  fetchInvitedContracts: roleId => dispatch(fetchInvitedContracts(roleId)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InvitedContractsContainer);
