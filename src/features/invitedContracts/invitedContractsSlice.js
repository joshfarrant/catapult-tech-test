import { createSlice } from '@reduxjs/toolkit';

import { fetchInvitedContractsList } from '../../lib/requests/webapi';

const invitedContractsSlice = createSlice({
  name: 'invitedContracts',
  initialState: {
    invitedContractsList: {},
    fetchingInvitedContracts: false,
  },
  /**
   * We're intentionally mutating the state here
   * https://redux-toolkit.js.org/tutorials/intermediate-tutorial#mutable-update-logic
   */
  /* eslint-disable no-param-reassign */
  reducers: {
    fetchInvitedContractsFulfilled: (state, action) => {
      const {
        payload: { invitedContracts },
      } = action;

      invitedContracts.forEach(invitedContract => {
        state.invitedContractsList[invitedContract.id] = invitedContract;
      });

      state.fetchingInvitedContracts = false;
    },
    fetchingInvitedContracts: state => {
      state.fetchingInvitedContracts = true;
    },
  },
  /* eslint-enable no-param-reassign */
});

export const {
  fetchInvitedContractsFulfilled,
  fetchingInvitedContracts,
} = invitedContractsSlice.actions;

export default invitedContractsSlice.reducer;

export const fetchInvitedContracts = () => async dispatch => {
  dispatch(fetchingInvitedContracts());

  try {
    const invitedContracts = await fetchInvitedContractsList();

    const data = {
      invitedContracts: invitedContracts.data,
    };

    dispatch(fetchInvitedContractsFulfilled(data));
  } catch (err) {
    // TODO Add proper error handling
    // eslint-disable-next-line no-console
    console.error('Error fetching invited contracts: ', err);
  }
};
