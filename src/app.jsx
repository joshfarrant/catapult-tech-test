import React from 'react';
import { hot } from 'react-hot-loader';
import { Provider } from 'react-redux';

import store from './reducers';
import ShiftsContainer from './features/shifts/ShiftsContainer';

const App = () => {
  return (
    <Provider store={store}>
      <ShiftsContainer />
    </Provider>
  );
};

export default hot(module)(App);
