import { combineReducers } from 'redux';

import createStore from './createStore';
import invitedContractsReducer from '../features/invitedContracts/invitedContractsSlice';
import shiftsReducer from '../features/shifts/shiftsSlice';

const catapultReducer = combineReducers({
  invitedContracts: invitedContractsReducer,
  shifts: shiftsReducer,
});

export default createStore(catapultReducer);
