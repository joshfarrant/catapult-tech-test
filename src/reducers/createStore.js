import { configureStore } from '@reduxjs/toolkit';

// creates the store
export default rootReducer => {
  const store = configureStore({
    reducer: rootReducer,
  });

  return store;
};
