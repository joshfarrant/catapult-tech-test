module.exports = {
  extends: ['airbnb', 'prettier', 'eslint-config-prettier'],
  parser: 'babel-eslint',
  env: {
    browser: true,
    es6: true,
    jest: true,
  },
  rules: {
    'import/prefer-default-export': 0,
    'jsx-a11y/label-has-for': 0,
  },
};
